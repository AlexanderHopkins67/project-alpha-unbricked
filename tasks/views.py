from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTaskForm
from tasks.models import Task


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home")

    else:
        form = CreateTaskForm()

    context = {
        "form": form,
    }
    return render(request, "pages/create_task.html", context)


@login_required
def all_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)

    context = {"tasks": tasks}

    return render(request, "pages/my_tasks.html", context)
