from django.urls import path
from tasks.views import create_project, all_my_tasks


urlpatterns = [
    path("create/", create_project, name="create_task"),
    path("mine/", all_my_tasks, name="show_my_tasks"),
]
